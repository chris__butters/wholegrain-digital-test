# Wholegrain Digital Backend test

Creates a custom post type that can be accessed via AJAX

Utilises the REST API to output content and can be publicly accessed

Follows PSR2 standards throughout and tries to bring order to WordPress using OOP.

### Written but not tested

This is down to time constraints but would like to extend to create the custom meta box manually instead of relying on user input and potentially use 'ingredients' as tags so the API can be queried by ingredient to help find a suitable recipe.

#### Requires PHP7
