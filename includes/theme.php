<?php
/**
  * @desc this class will hold functions to create the custom post type
  * and extend the API to include a custom field names 'ingredients'
  * @author Chris Butterworth chris@code-designs.co.uk
*/

namespace CustomTheme;

class CustomTheme
{
    public function __construct()
    {
        add_action('init', [$this, 'createRecipePostType']);
        add_action('rest_api_init', [$this, 'addMetaFieldtoApi']);
    }
    private function createRecipePostType()
    {
        $labels = array(
            'name'               =>
                _x('Recipes', 'post type general name', 'custom-theme'),
            'singular_name'      =>
                _x('Recipe', 'post type singular name', 'custom-theme'),
            'menu_name'          =>
                _x('Recipes', 'admin menu', 'custom-theme'),
            'name_admin_bar'     =>
                _x('Recipes', 'add new on admin bar', 'custom-theme'),
            'add_new'            =>
                _x('Add New', 'recipe', 'custom-theme'),
            'add_new_item'       =>
                __('Add New Recipe', 'custom-theme'),
            'new_item'           =>
                __('New Recipe', 'custom-theme'),
            'edit_item'          =>
                __('Edit Recipe', 'custom-theme'),
            'view_item'          =>
                __('View Recipe', 'custom-theme'),
            'all_items'          =>
                __('All Recipes', 'custom-theme'),
            'search_items'       =>
                __('Search Recipes', 'custom-theme'),
            'parent_item_colon'  =>
                __('Parent Recipes:', 'custom-theme'),
            'not_found'          =>
                __('No recipe found.', 'custom-theme'),
            'not_found_in_trash' =>
                __('No recipe found in Trash.', 'custom-theme')
        );
         
        $args = array(
            'labels'             => $labels,
            'description'        => __('Description.', 'custom-theme'),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array('slug' => 'recipe'),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'show_in_rest'       => true,
            'rest_base'          => 'recipe',
            'rest_controller_class' => 'WP_REST_Posts_Controller',
            'supports'           => array('title', 'editor', 'author', 'thumbnail', 'custom-fields')
        );
         
        register_post_type('recipe', $args);
    }
    private function addMetaFieldtoApi()
    {
        register_rest_field('recipe', 'ingredients', array(
           'get_callback'    => [$this,'getIngredients'],
           'schema'          => null,
        );
    }
    private function getIngredients($object)
    {
        return get_post_meta($object['id'], 'ingredients', true);
    }
}
