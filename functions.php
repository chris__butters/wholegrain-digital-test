<?php
/**
  * @desc this file creates a new instance of the custom theme class
  * @author Chris Butterworth chris@code-designs.co.uk
  * @required includes/theme.php
*/

require 'includes/theme.php';

$theme = new CustomTheme();
